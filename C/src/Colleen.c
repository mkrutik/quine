#include <stdio.h>
/*
    Comment outside function.
*/
void fuu()
{
    return ;
}

int main()
{
    char *s = "#include <stdio.h>%1$c/*%1$c    Comment outside function.%1$c*/%1$cvoid fuu()%1$c{%1$c    return ;%1$c}%1$c%1$cint main()%1$c{%1$c    char *s = %2$c%3$s%2$c;%1$c    printf(s,10,34,s);%1$c    /*%1$c        Comment inside function.%1$c    */%1$c    fuu();%1$c    return (0);%1$c}";
    printf(s,10,34,s);
    /*
        Comment inside function.
    */
    fuu();
    return (0);
}