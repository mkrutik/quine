#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
int main()
{
    char buff[1000];
    char name[100];
    int i = 5;
    #ifdef INC
    i--;
    #endif
    char *s = "#include <stdlib.h>%4$c#include <stdio.h>%4$c#include <fcntl.h>%4$c#include <unistd.h>%4$cint main()%4$c{%4$c    char buff[1000];%4$c    char name[100];%4$c    int i = %7$d;%4$c    #ifdef INC%4$c    i--;%4$c    #endif%4$c    char *s = %5$c%6$s%5$c;%4$c    sprintf(buff, %5$cSully_%1$cd.c%5$c, i);%4$c    int fd = open(buff, 01 | 0100, 0666);%4$c    if (fd != -1)%4$c    {%4$c        dprintf(fd,s,37,37,37,10,34,s,i);%4$c        close(fd);%4$c        sprintf(name, %5$c ./Sully_%1$cd%5$c, i);%4$c        sprintf(buff, %5$c clang -Wall -Wextra -Werror -D INC Sully_%cd.c -o Sully_%cd ;%cs%5$c, i, i, (i > 0) ? name :%5$c%5$c);%4$c        system(buff);%4$c    }%4$c    return (0);%4$c}";
    sprintf(buff, "Sully_%d.c", i);
    int fd = open(buff, 01 | 0100, 0666);
    if (fd != -1)
    {
        dprintf(fd,s,37,37,37,10,34,s,i);
        close(fd);
        sprintf(name, " ./Sully_%d", i);
        sprintf(buff, " clang -Wall -Wextra -Werror -D INC Sully_%d.c -o Sully_%d ;%s", i, i, (i > 0) ? name :"");
        system(buff);
    }
    return (0);
}