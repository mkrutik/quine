#include <string>
#include <fstream>
#include <cstdlib>
#include <boost/format.hpp>

int main()
{
    int i = 5;
#ifdef INC
    i--;
#endif
    std::string f_all = "#include <string>%1%#include <fstream>%1%#include <cstdlib>%1%#include <boost/format.hpp>%1%%1%int main()%1%{%1%    int i = %4%;%1%#ifdef INC%1%    i--;%1%#endif%1%    std::string f_all = %2%%3%%2%;%1%%1%    std::string f_name = %2%Sully_%%1%%.cpp%2%;%1%    std::string f_bin_name = %2%./Sully_%%1%%%2%;%1%    std::string f_execute = %2%clang++ -Wall -Wextra -Werror -D INC Sully_%%1%%.cpp -o Sully_%%1%%; %%2%%%2%;%1%%1%    auto src_name = boost::format{f_name.c_str()} %% i;%1%    std::ofstream file;%1%    file.open(src_name.str(), std::ios::out);%1%    if (file.is_open())%1%    {%1%        file << boost::format{f_all.c_str()} %% (char)10 %% '%2%' %% f_all.c_str() %% i;%1%        file.close();%1%%1%        auto bin = boost::format{f_bin_name.c_str()} %% i;%1%        auto execute = boost::format{f_execute.c_str()} %% i %% (i > 0 ? bin.str() : %2%%2%);%1%        std::system(execute.str().c_str());%1%    }%1%    return (0);%1%}";

    std::string f_name = "Sully_%1%.cpp";
    std::string f_bin_name = "./Sully_%1%";
    std::string f_execute = "clang++ -Wall -Wextra -Werror -D INC Sully_%1%.cpp -o Sully_%1%; %2%";

    auto src_name = boost::format{f_name.c_str()} % i;
    std::ofstream file;
    file.open(src_name.str(), std::ios::out);
    if (file.is_open())
    {
        file << boost::format{f_all.c_str()} % (char)10 % '"' % f_all.c_str() % i;
        file.close();

        auto bin = boost::format{f_bin_name.c_str()} % i;
        auto execute = boost::format{f_execute.c_str()} % i % (i > 0 ? bin.str() : "");
        std::system(execute.str().c_str());
    }
    return (0);
}