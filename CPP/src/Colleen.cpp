#include <iostream>
#include <boost/format.hpp>

/*
    Comment outside function.
*/
void fuu(void)
{
    return ;
}

int main()
{
    fuu();
    /*
        Comment inside function.
    */
    std::string  f = "#include <iostream>%1%#include <boost/format.hpp>%1%%1%/*%1%    Comment outside function.%1%*/%1%void fuu(void)%1%{%1%    return ;%1%}%1%%1%int main()%1%{%1%    fuu();%1%    /*%1%        Comment inside function.%1%    */%1%    std::string  f = %2%%3%%2%;%1%    std::cout << boost::format{f} %% (char)10 %% '%2%' %% f;%1%    return (0);%1%}";
    std::cout << boost::format{f} % (char)10 % '"' % f;
    return (0);
}