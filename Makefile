# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mkrutik <mkrutik@student.unit.ua>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/03 18:56:52 by mkrutik           #+#    #+#              #
#    Updated: 2019/08/07 20:20:06 by mkrutik          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

.PHONY: all clean fclean re

all:
	make -C ./C
	make -C ./ASM
	make -C ./CPP

clean:
	make -C ./C clean
	make -C ./ASM clean
	make -C ./CPP clean

fclean: clean
	make -C ./C fclean
	make -C ./ASM fclean
	make -C ./CPP fclean

re:
	make -C ./C re
	make -C ./ASM re
	make -C ./CPP re