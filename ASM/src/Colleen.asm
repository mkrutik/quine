extern printf

section .text
    global main

; Like function
func:
    ret

main:
    enter   0, 0

    call    func
    mov     rdi,    f
    mov     rsi,    10
    mov     rdx,    34
    mov     rcx,    f
    call    printf

    leave
    mov     rax,    0
    ret

    ; format string
    f db "extern printf%1$c%1$csection .text%1$c    global main%1$c%1$c; Like function%1$cfunc:%1$c    ret%1$c%1$cmain:%1$c    enter   0, 0%1$c%1$c    call    func%1$c    mov     rdi,    f%1$c    mov     rsi,    10%1$c    mov     rdx,    34%1$c    mov     rcx,    f%1$c    call    printf%1$c%1$c    leave%1$c    mov     rax,    0%1$c    ret%1$c%1$c    ; format string%1$c    f db %2$c%3$s%2$c, 0x0", 0x0