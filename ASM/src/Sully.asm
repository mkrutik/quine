extern dprintf
extern sprintf
extern system

section .text
    global main

main:
    enter   0, 0

    mov     r15,    5
%ifdef INC
    sub     r15,    1
%endif

    mov     rdi,    f_file
    mov     rsi,    f_file
    mov     rdx,    r15
    mov     rax,    0
    call    sprintf

    cmp     rax, 0
    jl      error

    mov     rdi,    f_exec
    mov     rsi,    f_exec
    mov     rdx,    r15
    mov     rax,    0
    call    sprintf

    cmp     rax, 0
    jl      error

    cmp     r15, 0
    je      stop

    lea     rdi,    [rel f_execuve]
    lea     rsi,    [rel f_execuve]
    mov     rdx,    r15
    lea     rcx,    [rel f_exec]
    mov     rax,    0
    call    sprintf

    cmp     rax, 0
    jl      error
    jmp     open

stop:
    lea     rdi,    [rel f_execuve]
    lea     rsi,    [rel f_execuve]
    mov     rdx,    r15
    lea     rcx,    [rel empty]
    call    sprintf

    cmp     rax, 0
    jl      error

open:
    mov     rdi,    f_file
    mov     rsi,    0q101
    mov     rdx,    0q666
    mov     rax,    2
    syscall

    cmp     rax,    2
    jl      error

    push    rax

print_to_file:

    mov     rdi,    rax
    lea     rsi,    [rel f]
    mov     rdx,    10
    mov     rcx,    34
    mov     r8,     r15
    lea     r9,     [rel f]
    mov     rax,    0x0
    call    dprintf

close:
    pop     rdi
    mov     rax,    3
    syscall

    lea     rdi,    [rel f_execuve]
    call    system

return:
    leave
    mov     rax,    0
    ret

error:
    leave
    mov     rax,    1
    ret

section .data
    empty db "", 0x0
    f_file db "Sully_%d.asm", 0x0
    dummy_1 times 20 db ' '
    f_exec db "./Sully_%d", 0x0
    dummy_2 times 20 db ' '
    f_execuve db "nasm -f elf64 -dINC=1 Sully_%1$d.asm -o Sully_%1$d.o && clang Sully_%1$d.o -o Sully_%1$d && rm -f Sully_%1$d.o; %2$s", 0x0
    dummy_3 times 200 db ' '
    f db "extern dprintf%1$cextern sprintf%1$cextern system%1$c%1$csection .text%1$c    global main%1$c%1$cmain:%1$c    enter   0, 0%1$c%1$c    mov     r15,    %3$d%1$c%%ifdef INC%1$c    sub     r15,    1%1$c%%endif%1$c%1$c    mov     rdi,    f_file%1$c    mov     rsi,    f_file%1$c    mov     rdx,    r15%1$c    mov     rax,    0%1$c    call    sprintf%1$c%1$c    cmp     rax, 0%1$c    jl      error%1$c%1$c    mov     rdi,    f_exec%1$c    mov     rsi,    f_exec%1$c    mov     rdx,    r15%1$c    mov     rax,    0%1$c    call    sprintf%1$c%1$c    cmp     rax, 0%1$c    jl      error%1$c%1$c    cmp     r15, 0%1$c    je      stop%1$c%1$c    lea     rdi,    [rel f_execuve]%1$c    lea     rsi,    [rel f_execuve]%1$c    mov     rdx,    r15%1$c    lea     rcx,    [rel f_exec]%1$c    mov     rax,    0%1$c    call    sprintf%1$c%1$c    cmp     rax, 0%1$c    jl      error%1$c    jmp     open%1$c%1$cstop:%1$c    lea     rdi,    [rel f_execuve]%1$c    lea     rsi,    [rel f_execuve]%1$c    mov     rdx,    r15%1$c    lea     rcx,    [rel empty]%1$c    call    sprintf%1$c%1$c    cmp     rax, 0%1$c    jl      error%1$c%1$copen:%1$c    mov     rdi,    f_file%1$c    mov     rsi,    0q101%1$c    mov     rdx,    0q666%1$c    mov     rax,    2%1$c    syscall%1$c%1$c    cmp     rax,    2%1$c    jl      error%1$c%1$c    push    rax%1$c%1$cprint_to_file:%1$c%1$c    mov     rdi,    rax%1$c    lea     rsi,    [rel f]%1$c    mov     rdx,    10%1$c    mov     rcx,    34%1$c    mov     r8,     r15%1$c    lea     r9,     [rel f]%1$c    mov     rax,    0x0%1$c    call    dprintf%1$c%1$cclose:%1$c    pop     rdi%1$c    mov     rax,    3%1$c    syscall%1$c%1$c    lea     rdi,    [rel f_execuve]%1$c    call    system%1$c%1$creturn:%1$c    leave%1$c    mov     rax,    0%1$c    ret%1$c%1$cerror:%1$c    leave%1$c    mov     rax,    1%1$c    ret%1$c%1$csection .data%1$c    empty db %2$c%2$c, 0x0%1$c    f_file db %2$cSully_%%d.asm%2$c, 0x0%1$c    dummy_1 times 20 db ' '%1$c    f_exec db %2$c./Sully_%%d%2$c, 0x0%1$c    dummy_2 times 20 db ' '%1$c    f_execuve db %2$cnasm -f elf64 -dINC=1 Sully_%%1$d.asm -o Sully_%%1$d.o && clang Sully_%%1$d.o -o Sully_%%1$d && rm -f Sully_%%1$d.o; %%2$s%2$c, 0x0%1$c    dummy_3 times 200 db ' '%1$c    f db %2$c%4$s%2$c, 0x0", 0x0