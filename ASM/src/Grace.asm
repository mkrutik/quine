%define NAME "Grace_kid.c"
%define PERM 0q101
%macro FT 0

extern dprintf

section .text
    global main

main:
    enter   0, 0

open:
    mov     rdi,    fname
    mov     rsi,    PERM
    mov     rdx,    0q666
    mov     rax,    2
    syscall

    cmp     rax,    2
    jl      return

    push    rax

print_to_file:

    mov     rdi,    rax
    lea     rsi,    [rel f]
    mov     rdx,    10
    mov     rcx,    34
    lea     r8,     [rel f]
    mov     rax,    0x0
    call    dprintf

close:
    pop     rdi
    mov     rax,    3
    syscall

return:
    leave
    mov     rax,    0
    ret

    fname db NAME, 0x0

section .data
    ; format string
    f db "%%define NAME %2$cGrace_kid.c%2$c%1$c%%define PERM 0q101%1$c%%macro FT 0%1$c%1$cextern dprintf%1$c%1$csection .text%1$c    global main%1$c%1$cmain:%1$c    enter   0, 0%1$c%1$copen:%1$c    mov     rdi,    fname%1$c    mov     rsi,    PERM%1$c    mov     rdx,    0q666%1$c    mov     rax,    2%1$c    syscall%1$c%1$c    cmp     rax,    2%1$c    jl      return%1$c%1$c    push    rax%1$c%1$cprint_to_file:%1$c%1$c    mov     rdi,    rax%1$c    lea     rsi,    [rel f]%1$c    mov     rdx,    10%1$c    mov     rcx,    34%1$c    lea     r8,     [rel f]%1$c    mov     rax,    0x0%1$c    call    dprintf%1$c%1$cclose:%1$c    pop     rdi%1$c    mov     rax,    3%1$c    syscall%1$c%1$creturn:%1$c    leave%1$c    mov     rax,    0%1$c    ret%1$c%1$c    fname db NAME, 0x0%1$c%1$csection .data%1$c    ; format string%1$c    f db %2$c%3$s%2$c, 0x0%1$c%1$c%%endmacro%1$c%1$cFT", 0x0

%endmacro

FT